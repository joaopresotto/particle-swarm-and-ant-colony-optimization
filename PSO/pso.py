# João Gabriel Camacho Presotto
# Trabalho 3 - Computação Inspirada pela Natureza
# Exercício PSO

import functions_pso as functions
import time
import numpy as np
import os
from progressbar import ProgressBar

# f(x, y) = (1 − x)^2 + 100(y − x^2)^2
# tem mínimo global f(x,y) = 0 em x = 1 e y = 1
def funct(x,y):
    return ((1-x) ** 2) + (100*(y-(x**2))**2)

# Função de aptidão
def aptitude_usingFunctionValue(individual):
    return round(funct(individual[0],individual[1]),4)

# PSO
def compute_pso(pop_size, max_it, ac_1, ac_2, v_max, v_min, stop_when_min):
    # Estruturas para armazenar valor minimo e medio de f(x,y) ao longo das iterações
    fs_min = []
    fs_med = []

    # Inicializando enxame X = [ (x0,y0), ... , (xn,yn)]
    X = np.random.uniform(-5,5,pop_size*2)
    X = np.reshape(X, (pop_size,2))

    # Inicializando V = [ (Vx_0,Vy_0), ..., (Vx_n,Vy_n)]
    V = np.random.uniform(v_min,v_max,pop_size*2)
    V = np.reshape(V, (pop_size,2))

    # Estrutura para armazenar aptidão dos indivíduos
    best_aptitudes = np.array([[X[i],aptitude_usingFunctionValue(X[i])] for i in range(pop_size)])

    # Criando estrutura de vizinhança
    # [ [indice_individuo, indice_melhor_vizinho, [vizinhos]],
    #   ...
    # ]
    neighborhood = np.array([[i,i,[i-1,i+1]] for i in range(pop_size)])
    neighborhood[:,2][pop_size-1][1] = 0 # o vizinho a direita do ultimo elemento é o primeiro (indice 0)

    for t in range(max_it):
        fs_min.append(best_aptitudes[best_aptitudes[:,1].argmin(),1])
        fs_med.append(np.mean(best_aptitudes[:,1], dtype=np.float64))

        for i in range(pop_size):
            # Melhor desempenho individual
            current_aptitude = aptitude_usingFunctionValue(X[i])
            if current_aptitude < best_aptitudes[i,1]:
                best_aptitudes[i] = [X[i],current_aptitude]

            # Vizinhança
            for j in neighborhood[i,2]:
                current_neighbor_aptitude = aptitude_usingFunctionValue(X[j])
                if current_neighbor_aptitude < best_aptitudes[neighborhood[i,1],1]:
                    neighborhood[i,1] = j

            # Atualiza velocidades
            V = V + (np.random.uniform(0,ac_1,pop_size*2).reshape((pop_size,2)) * (best_aptitudes[i,0] - X[i])) + (np.random.uniform(0,ac_2,pop_size*2).reshape((pop_size,2)) * (best_aptitudes[neighborhood[i,1],0] - X[i]))
            V = np.clip(V,v_min,v_max)

            # Atualiza as posições dos indivíduos
            X = X + V 
            X = np.clip(X,-5,5)   

        # Se encontrar o mínimo global f(x,y) = 0, para a execução do PSO
        if stop_when_min and (0.0 in fs_min):
            break        

    # Ao fim das iterações, retorna o indivíduo mais apto encontrado e os valores mínimos e médios de f(x,y) obtidos
    best = best_aptitudes[:,1].argmin()
    return best_aptitudes[best,0], fs_min, fs_med

#*******************************************************
# 						  MAIN
#*******************************************************
def main():
    try:
        os.makedirs("Resultados Experimentais/")
    except FileExistsError:
        # directory already exists
        pass

    start = time.time()

    max_it = 250
    pop_size = [(i+1)*10 for i in range(5)]

    # Variaveis para gerar os gráficos
    exec_timeA,exec_timeB, exec_timeC, exec_timeD, exec_timeE = ([] for i in range(5))
    num_iter_totalA, num_iter_totalB, num_iter_totalC, num_iter_totalD, num_iter_totalE = ([] for i in range(5))
    fs_min_meanA, fs_min_meanB, fs_min_meanC, fs_min_meanD, fs_min_meanE = ([] for i in range(5))
    fs_med_meanA, fs_med_meanB, fs_med_meanC, fs_med_meanD, fs_med_meanE = ([] for i in range(5))

    p_bar = ProgressBar()
    for i in p_bar(range(100)):
        # Chamadas do PSO
        # n_particulas = 10
        startA = time.time()
        PA, fs_minA, fs_medA = compute_pso(pop_size[0],max_it,ac_1=2.05,ac_2=2.05,v_max=2,v_min=-2,stop_when_min=True)
        endA = time.time()

        # n_particulas = 20
        startB = time.time()
        PB, fs_minB, fs_medB = compute_pso(pop_size[1],max_it,ac_1=2.05,ac_2=2.05,v_max=2,v_min=-2,stop_when_min=True)
        endB = time.time()
        
        # n_particulas = 30
        startC = time.time()
        PC, fs_minC, fs_medC = compute_pso(pop_size[2],max_it,ac_1=2.05,ac_2=2.05,v_max=2,v_min=-2,stop_when_min=True)
        endC = time.time()

        # n_particulas = 40
        startD = time.time()
        PD, fs_minD, fs_medD = compute_pso(pop_size[3],max_it,ac_1=2.05,ac_2=2.05,v_max=2,v_min=-2,stop_when_min=True)
        endD = time.time()

        # n_particulas = 50
        startE = time.time()
        PE, fs_minE, fs_medE = compute_pso(pop_size[4],max_it,ac_1=2.05,ac_2=2.05,v_max=2,v_min=-2,stop_when_min=True)
        endE = time.time()

        # Gráficos da execução 50
        if i == 50:
            functions.plot_fs(fs_minA,fs_medA,pop_size[0],file_path="Resultados Experimentais/exec50_10.pdf")
            functions.plot_fs(fs_minB,fs_medB,pop_size[1],file_path="Resultados Experimentais/exec50_20.pdf")
            functions.plot_fs(fs_minC,fs_medC,pop_size[2],file_path="Resultados Experimentais/exec50_30.pdf")
            functions.plot_fs(fs_minD,fs_medD,pop_size[3],file_path="Resultados Experimentais/exec50_40.pdf")
            functions.plot_fs(fs_minE,fs_medE,pop_size[4],file_path="Resultados Experimentais/exec50_50.pdf")

        fs_min_meanA, fs_med_meanA, num_iter_totalA, exec_timeA = functions.append_values(fs_min_meanA,fs_med_meanA,fs_minA,fs_medA,num_iter_totalA,len(fs_minA),exec_timeA,startA,endA)
        fs_min_meanB, fs_med_meanB, num_iter_totalB, exec_timeB = functions.append_values(fs_min_meanB,fs_med_meanB,fs_minB,fs_medB,num_iter_totalB,len(fs_minB),exec_timeB,startB,endB)
        fs_min_meanC, fs_med_meanC, num_iter_totalC, exec_timeC = functions.append_values(fs_min_meanC,fs_med_meanC,fs_minC,fs_medC,num_iter_totalC,len(fs_minC),exec_timeC,startC,endC)
        fs_min_meanD, fs_med_meanD, num_iter_totalD, exec_timeD = functions.append_values(fs_min_meanD,fs_med_meanD,fs_minD,fs_medD,num_iter_totalD,len(fs_minD),exec_timeD,startD,endD)
        fs_min_meanE, fs_med_meanE, num_iter_totalE, exec_timeE = functions.append_values(fs_min_meanE,fs_med_meanE,fs_minE,fs_medE,num_iter_totalE,len(fs_minE),exec_timeE,startE,endE)

    num_iters = functions.append_num_iters(num_iter_totalA,num_iter_totalB,num_iter_totalC,num_iter_totalD,num_iter_totalE)
    exec_times = functions.append_exec_times(exec_timeA,exec_timeB,exec_timeC,exec_timeD,exec_timeE)

    # Estatisticas
    # n_particulas = 10
    functions.write_statistics(pop_size[0],fs_med_meanA,num_iters[0],exec_times[0],file_path="Resultados Experimentais/estatisticasPSO.txt")
    # n_particulas = 20
    functions.write_statistics(pop_size[1],fs_med_meanB,num_iters[1],exec_times[1],file_path="Resultados Experimentais/estatisticasPSO.txt")
    # n_particulas = 30
    functions.write_statistics(pop_size[2],fs_med_meanC,num_iters[2],exec_times[2],file_path="Resultados Experimentais/estatisticasPSO.txt")
    # n_particulas = 40
    functions.write_statistics(pop_size[3],fs_med_meanD,num_iters[3],exec_times[3],file_path="Resultados Experimentais/estatisticasPSO.txt")
    # n_particulas = 50
    functions.write_statistics(pop_size[4],fs_med_meanE,num_iters[4],exec_times[4],file_path="Resultados Experimentais/estatisticasPSO.txt")

    # Gráficos de média de iterações para encontrar minimo de f(x,y)
    functions.plot_fxy_over_time(num_iters,pop_size,file_path="Resultados Experimentais/geracoesPorExecucaoPSO.pdf")
    
    # Grafíco de tempo para encontrar minimo de f(x,y)
    #exec_times = functions.append_exec_times(exec_timeA[:25],exec_timeB[:25],exec_timeC[:25],exec_timeD[:25],exec_timeE[:25])
    functions.plot_execution_time_over_time(exec_times,pop_size,file_path="Resultados Experimentais/tempoPorExecucaoPSO.pdf")

    end = time.time()
    print("execution time:", round(end-start,4),"seconds.")

if __name__ == "__main__":
    main()